<?php
/**
 * An helper file for Orchestra Platform, to provide autocomplete information to your IDE
 * Generated with https://github.com/barryvdh/laravel-ide-helper
 *
 * Edited by Alvaro Canepa <bfpdevel@gmail.com>
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */

namespace Orchestra{
	class Asset extends \Orchestra\Support\Facades\Asset{
		/**
		 * Construct a new environment.
		 *
		 * @param Dispatcher  $dispatcher
		 * @static 
		 */
		 public static function __construct($dispatcher){
			//Method inherited from \Orchestra\Asset\Environment
			 \Orchestra\Asset\Environment::__construct($dispatcher);
		 }

		/**
		 * Get an asset container instance.
		 * 
		 * <code>
		 *     // Get the default asset container
		 *     $container = Orchestra\Asset::container();
		 * 
		 *     // Get a named asset container
		 *     $container = Orchestra\Asset::container('footer');
		 * </code>
		 *
		 * @param string   $container
		 * @return Container
		 * @static 
		 */
		 public static function container($container = 'default'){
			//Method inherited from \Orchestra\Asset\Environment
			return \Orchestra\Asset\Environment::container($container);
		 }

		/**
		 * Magic Method for calling methods on the default container.
		 * 
		 * <code>
		 *     // Call the "styles" method on the default container
		 *     echo Orchestra\Asset::styles();
		 * 
		 *     // Call the "add" method on the default container
		 *     Orchestra\Asset::add('jquery', 'js/jquery.js');
		 * </code>
		 *
		 * @param string   $method
		 * @param array    $parameters
		 * @return mixed
		 * @static 
		 */
		 public static function __call($method, $parameters){
			//Method inherited from \Orchestra\Asset\Environment
			return \Orchestra\Asset\Environment::__call($method, $parameters);
		 }

	}
	class Acl extends \Orchestra\Support\Facades\Acl{
		/**
		 * Construct a new Environment.
		 *
		 * @param \Illuminate\Auth\Guard   $auth
		 * @static 
		 */
		 public static function __construct($auth){
			//Method inherited from \Orchestra\Auth\Acl\Environment
			 \Orchestra\Auth\Acl\Environment::__construct($auth);
		 }

		/**
		 * Initiate a new ACL Container instance.
		 *
		 * @param string                      $name
		 * @param \Orchestra\Memory\Provider  $memory
		 * @return Container
		 * @static 
		 */
		 public static function make($name = null, $memory = null){
			//Method inherited from \Orchestra\Auth\Acl\Environment
			return \Orchestra\Auth\Acl\Environment::make($name, $memory);
		 }

		/**
		 * Register an ACL Container instance with Closure.
		 *
		 * @param string   $name
		 * @param \Closure $callback
		 * @return Container
		 * @static 
		 */
		 public static function register($name, $callback = null){
			//Method inherited from \Orchestra\Auth\Acl\Environment
			return \Orchestra\Auth\Acl\Environment::register($name, $callback);
		 }

		/**
		 * Manipulate and synchronize roles.
		 *
		 * @param string   $method
		 * @param array    $parameters
		 * @return mixed
		 * @static 
		 */
		 public static function __call($method, $parameters){
			//Method inherited from \Orchestra\Auth\Acl\Environment
			return \Orchestra\Auth\Acl\Environment::__call($method, $parameters);
		 }

		/**
		 * Shutdown/finish all ACL.
		 *
		 * @return Environment
		 * @static 
		 */
		 public static function finish(){
			//Method inherited from \Orchestra\Auth\Acl\Environment
			return \Orchestra\Auth\Acl\Environment::finish();
		 }

		/**
		 * Get all ACL instances.
		 *
		 * @return array
		 * @static 
		 */
		 public static function all(){
			//Method inherited from \Orchestra\Auth\Acl\Environment
			return \Orchestra\Auth\Acl\Environment::all();
		 }

		/**
		 * Get ACL instance by name.
		 *
		 * @return Container
		 * @static 
		 */
		 public static function get($name){
			//Method inherited from \Orchestra\Auth\Acl\Environment
			return \Orchestra\Auth\Acl\Environment::get($name);
		 }

	}
	class App extends \Orchestra\Support\Facades\App{
		/**
		 * Start the application.
		 *
		 * @return Application
		 * @static 
		 */
		 public static function boot(){
			//Method inherited from \Orchestra\Foundation\Application
			return \Orchestra\Foundation\Application::boot();
		 }

		/**
		 * Get Application instance.
		 *
		 * @return \Illuminate\Foundation\Application
		 * @static 
		 */
		 public static function illuminate(){
			//Method inherited from \Orchestra\Foundation\Application
			return \Orchestra\Foundation\Application::illuminate();
		 }

		/**
		 * Get installation status.
		 *
		 * @return boolean
		 * @static 
		 */
		 public static function installed(){
			//Method inherited from \Orchestra\Foundation\Application
			return \Orchestra\Foundation\Application::installed();
		 }

		/**
		 * Magic method to get services.
		 *
		 * @param string   $method
		 * @param array    $parameters
		 * @return mixed
		 * @static 
		 */
		 public static function __call($method, $parameters){
			//Method inherited from \Orchestra\Foundation\Application
			return \Orchestra\Foundation\Application::__call($method, $parameters);
		 }

		/**
		 * Construct a new instance.
		 *
		 * @param \Illuminate\Foundation\Application   $app
		 * @return void
		 * @static 
		 */
		 public static function __construct($app){
			//Method inherited from \Orchestra\Foundation\Abstractable\RouteManager
			 \Orchestra\Foundation\Application::__construct($app);
		 }

		/**
		 * Return locate handles configuration for a package/app.
		 *
		 * @param string   $path
		 * @return array
		 * @static 
		 */
		 public static function locate($path){
			//Method inherited from \Orchestra\Foundation\Abstractable\RouteManager
			return \Orchestra\Foundation\Application::locate($path);
		 }

		/**
		 * Return route group dispatch for a package/app.
		 *
		 * @param string   $name   Package name
		 * @return string
		 * @static 
		 */
		 public static function group($name, $default, $group = array()){
			//Method inherited from \Orchestra\Foundation\Abstractable\RouteManager
			return \Orchestra\Foundation\Application::group($name, $default, $group);
		 }

		/**
		 * Return handles URL for a package/app.
		 *
		 * @param string   $path
		 * @return string
		 * @static 
		 */
		 public static function handles($path){
			//Method inherited from \Orchestra\Foundation\Abstractable\RouteManager
			return \Orchestra\Foundation\Application::handles($path);
		 }

		/**
		 * Return if handles URL match given string.
		 *
		 * @param string   $path
		 * @return boolean
		 * @static 
		 */
		 public static function is($path){
			//Method inherited from \Orchestra\Foundation\Abstractable\RouteManager
			return \Orchestra\Foundation\Application::is($path);
		 }

		/**
		 * Get extension route.
		 *
		 * @param string   $name
		 * @param string   $default
		 * @return string
		 * @static 
		 */
		 public static function route($name, $default = '/'){
			//Method inherited from \Orchestra\Foundation\Abstractable\RouteManager
			return \Orchestra\Foundation\Application::route($name, $default);
		 }

		/**
		 * Run the callback when route is matched.
		 *
		 * @param string   $path
		 * @param mixed    $listener
		 * @return void
		 * @static 
		 */
		 public static function when($path, $listener){
			//Method inherited from \Orchestra\Foundation\Abstractable\RouteManager
			 \Orchestra\Foundation\Application::when($path, $listener);
		 }

	}
	class Config extends \Orchestra\Support\Facades\Config{
		/**
		 * Construct a new ConfigManager instance.
		 *
		 * @param \Illuminate\Config\Repository    $config
		 * @param \Orchestra\Memory\MemoryManager  $memory
		 * @static 
		 */
		 public static function __construct($config, $memory){
			//Method inherited from \Orchestra\Extension\ConfigManager
			 \Orchestra\Extension\ConfigManager::__construct($config, $memory);
		 }

		/**
		 * Map configuration to allow orchestra to store it in database.
		 *
		 * @param string   $name
		 * @param array    $aliases
		 * @return boolean
		 * @static 
		 */
		 public static function map($name, $aliases){
			//Method inherited from \Orchestra\Extension\ConfigManager
			return \Orchestra\Extension\ConfigManager::map($name, $aliases);
		 }

	}
	class Extension extends \Orchestra\Support\Facades\Extension{
		/**
		 * Construct a new Application instance.
		 *
		 * @param \Illuminate\Container\Container $app
		 * @param Contracts\Dispatcher            $dispatcher
		 * @param Contracts\Debugger              $debugger
		 * @static 
		 */
		 public static function __construct($app, $dispatcher, $debugger){
			//Method inherited from \Orchestra\Extension\Environment
			 \Orchestra\Extension\Environment::__construct($app, $dispatcher, $debugger);
		 }

		/**
		 * Boot active extensions.
		 *
		 * @return Environment
		 * @static 
		 */
		 public static function boot(){
			//Method inherited from \Orchestra\Extension\Environment
			return \Orchestra\Extension\Environment::boot();
		 }

		/**
		 * Detect all extensions.
		 *
		 * @return array
		 * @static 
		 */
		 public static function detect(){
			//Method inherited from \Orchestra\Extension\Environment
			return \Orchestra\Extension\Environment::detect();
		 }

		/**
		 * Shutdown all extensions.
		 *
		 * @return Environment
		 * @static 
		 */
		 public static function finish(){
			//Method inherited from \Orchestra\Extension\Environment
			return \Orchestra\Extension\Environment::finish();
		 }

		/**
		 * Activate an extension.
		 *
		 * @param string   $name
		 * @return boolean
		 * @static 
		 */
		 public static function activate($name){
			//Method inherited from \Orchestra\Extension\Environment
			return \Orchestra\Extension\Environment::activate($name);
		 }

		/**
		 * Check whether an extension is active.
		 *
		 * @param string   $name
		 * @return boolean
		 * @static 
		 */
		 public static function activated($name){
			//Method inherited from \Orchestra\Extension\Environment
			return \Orchestra\Extension\Environment::activated($name);
		 }

		/**
		 * Check whether an extension is available.
		 *
		 * @param string   $name
		 * @return boolean
		 * @static 
		 */
		 public static function available($name){
			//Method inherited from \Orchestra\Extension\Environment
			return \Orchestra\Extension\Environment::available($name);
		 }

		/**
		 * Deactivate an extension.
		 *
		 * @param string   $name
		 * @return boolean
		 * @static 
		 */
		 public static function deactivate($name){
			//Method inherited from \Orchestra\Extension\Environment
			return \Orchestra\Extension\Environment::deactivate($name);
		 }

		/**
		 * Publish an extension.
		 *
		 * @param string
		 * @return void
		 * @static 
		 */
		 public static function publish($name){
			//Method inherited from \Orchestra\Extension\Environment
			 \Orchestra\Extension\Environment::publish($name);
		 }

		/**
		 * Get an option for a given extension.
		 *
		 * @param string   $name
		 * @param string   $option
		 * @param mixed    $default
		 * @return mixed
		 * @static 
		 */
		 public static function option($name, $option, $default = null){
			//Method inherited from \Orchestra\Extension\Environment
			return \Orchestra\Extension\Environment::option($name, $option, $default);
		 }

		/**
		 * Check whether an extension has a writable public asset.
		 *
		 * @param string   $name
		 * @return boolean
		 * @static 
		 */
		 public static function permission($name){
			//Method inherited from \Orchestra\Extension\Environment
			return \Orchestra\Extension\Environment::permission($name);
		 }

		/**
		 * Reset ectension.
		 *
		 * @param string   $name
		 * @return boolean
		 * @static 
		 */
		 public static function reset($name){
			//Method inherited from \Orchestra\Extension\Environment
			return \Orchestra\Extension\Environment::reset($name);
		 }

		/**
		 * Get extension route handle.
		 *
		 * @param string   $name
		 * @param string   $default
		 * @return string
		 * @static 
		 */
		 public static function route($name, $default = '/'){
			//Method inherited from \Orchestra\Extension\Environment
			return \Orchestra\Extension\Environment::route($name, $default);
		 }

		/**
		 * Check if extension is started.
		 *
		 * @param string   $name
		 * @return boolean
		 * @static 
		 */
		 public static function started($name){
			//Method inherited from \Orchestra\Extension\Environment
			return \Orchestra\Extension\Environment::started($name);
		 }

		/**
		 * Check whether a Memory instance is already attached to the container.
		 *
		 * @return boolean
		 * @static 
		 */
		 public static function attached(){
			//Method inherited from \Orchestra\Memory\Abstractable\Container
			return \Orchestra\Extension\Environment::attached();
		 }

		/**
		 * Attach memory provider.
		 *
		 * @param \Orchestra\Memory\Provider  $memory
		 * @return object
		 * @static 
		 */
		 public static function attach($memory){
			//Method inherited from \Orchestra\Memory\Abstractable\Container
			return \Orchestra\Extension\Environment::attach($memory);
		 }

		/**
		 * Set memory provider.
		 *
		 * @param \Orchestra\Memory\Provider  $memory
		 * @return object
		 * @static 
		 */
		 public static function setMemoryProvider($memory){
			//Method inherited from \Orchestra\Memory\Abstractable\Container
			return \Orchestra\Extension\Environment::setMemoryProvider($memory);
		 }

		/**
		 * Set memory provider.
		 *
		 * @return \Orchestra\Memory\Provider|null
		 * @static 
		 */
		 public static function getMemoryProvider(){
			//Method inherited from \Orchestra\Memory\Abstractable\Container
			return \Orchestra\Extension\Environment::getMemoryProvider();
		 }

	}
	class Form extends \Orchestra\Support\Facades\Form{
		/**
		 * Create a new Builder instance.
		 *
		 * @param \Closure $callback
		 * @return object
		 * @static 
		 */
		 public static function make($callback){
			//Method inherited from \Orchestra\Html\Form\Environment
			return \Orchestra\Html\Form\Environment::make($callback);
		 }

		/**
		 * Construct a new environment.
		 *
		 * @param \Illuminate\Container\Container  $app
		 * @static 
		 */
		 public static function __construct($app){
			//Method inherited from \Orchestra\Html\Abstractable\Environment
			 \Orchestra\Html\Form\Environment::__construct($app);
		 }

		/**
		 * Create a new builder instance of a named builder.
		 *
		 * @param string   $name
		 * @param \Closure $callback
		 * @return object
		 * @static 
		 */
		 public static function of($name, $callback = null){
			//Method inherited from \Orchestra\Html\Abstractable\Environment
			return \Orchestra\Html\Form\Environment::of($name, $callback);
		 }

	}
	class Mail extends \Orchestra\Support\Facades\Mail{
		/**
		 * Construct a new Mail instance.
		 *
		 * @param \Illuminate\Foundation\Application   $app
		 * @return void
		 * @static 
		 */
		 public static function __construct($app){
			//Method inherited from \Orchestra\Notifier\Mailer
			 \Orchestra\Notifier\Mailer::__construct($app);
		 }

		/**
		 * Allow Orchestra Platform to either use send or queue based on
		 * settings.
		 *
		 * @param string           $view
		 * @param array            $data
		 * @param Closure|string   $callback
		 * @param string           $queue
		 * @return \Illuminate\Mail\Mailer
		 * @static 
		 */
		 public static function push($view, $data, $callback, $queue = null){
			//Method inherited from \Orchestra\Notifier\Mailer
			return \Orchestra\Notifier\Mailer::push($view, $data, $callback, $queue);
		 }

		/**
		 * Force Orchestra Platform to send email directly.
		 *
		 * @param string           $view
		 * @param array            $data
		 * @param Closure|string   $callback
		 * @param string           $queue
		 * @return \Illuminate\Mail\Mailer
		 * @static 
		 */
		 public static function send($view, $data, $callback){
			//Method inherited from \Orchestra\Notifier\Mailer
			return \Orchestra\Notifier\Mailer::send($view, $data, $callback);
		 }

		/**
		 * Force Orchestra Platform to send email using queue.
		 *
		 * @param string           $view
		 * @param array            $data
		 * @param Closure|string   $callback
		 * @param string           $queue
		 * @return \Illuminate\Mail\Mailer
		 * @static 
		 */
		 public static function queue($view, $data, $callback, $queue = null){
			//Method inherited from \Orchestra\Notifier\Mailer
			return \Orchestra\Notifier\Mailer::queue($view, $data, $callback, $queue);
		 }

		/**
		 * Handle a queued e-mail message job.
		 *
		 * @param \Illuminate\Queue\Jobs\Job  $job
		 * @param array  $data
		 * @return void
		 * @static 
		 */
		 public static function handleQueuedMessage($job, $data){
			//Method inherited from \Orchestra\Notifier\Mailer
			 \Orchestra\Notifier\Mailer::handleQueuedMessage($job, $data);
		 }

		/**
		 * Check whether a Memory instance is already attached to the container.
		 *
		 * @return boolean
		 * @static 
		 */
		 public static function attached(){
			//Method inherited from \Orchestra\Memory\Abstractable\Container
			return \Orchestra\Notifier\Mailer::attached();
		 }

		/**
		 * Attach memory provider.
		 *
		 * @param \Orchestra\Memory\Provider  $memory
		 * @return object
		 * @static 
		 */
		 public static function attach($memory){
			//Method inherited from \Orchestra\Memory\Abstractable\Container
			return \Orchestra\Notifier\Mailer::attach($memory);
		 }

		/**
		 * Set memory provider.
		 *
		 * @param \Orchestra\Memory\Provider  $memory
		 * @return object
		 * @static 
		 */
		 public static function setMemoryProvider($memory){
			//Method inherited from \Orchestra\Memory\Abstractable\Container
			return \Orchestra\Notifier\Mailer::setMemoryProvider($memory);
		 }

		/**
		 * Set memory provider.
		 *
		 * @return \Orchestra\Memory\Provider|null
		 * @static 
		 */
		 public static function getMemoryProvider(){
			//Method inherited from \Orchestra\Memory\Abstractable\Container
			return \Orchestra\Notifier\Mailer::getMemoryProvider();
		 }

	}
	class Memory extends \Orchestra\Support\Facades\Memory{
		/**
		 * Get the default driver.
		 *
		 * @return string
		 * @static 
		 */
		 public static function getDefaultDriver(){
			//Method inherited from \Orchestra\Memory\MemoryManager
			return \Orchestra\Memory\MemoryManager::getDefaultDriver();
		 }

		/**
		 * Set the default driver.
		 *
		 * @param string   $name
		 * @return void
		 * @static 
		 */
		 public static function setDefaultDriver($name){
			//Method inherited from \Orchestra\Memory\MemoryManager
			 \Orchestra\Memory\MemoryManager::setDefaultDriver($name);
		 }

		/**
		 * Make default driver or fallback to runtime.
		 *
		 * @param string   $fallbackName
		 * @return \Orchestra\Memory\Provider
		 * @static 
		 */
		 public static function makeOrFallback($fallbackName = 'orchestra'){
			//Method inherited from \Orchestra\Memory\MemoryManager
			return \Orchestra\Memory\MemoryManager::makeOrFallback($fallbackName);
		 }

		/**
		 * Loop every instance and execute finish method (if available).
		 *
		 * @return void
		 * @static 
		 */
		 public static function finish(){
			//Method inherited from \Orchestra\Memory\MemoryManager
			 \Orchestra\Memory\MemoryManager::finish();
		 }

		/**
		 * Create a new instance.
		 *
		 * @param string   $driver
		 * @return object
		 * @see Manager::driver()
		 * @static 
		 */
		 public static function make($driver = null){
			//Method inherited from \Orchestra\Support\Manager
			return \Orchestra\Memory\MemoryManager::make($driver);
		 }

		/**
		 * Create a new manager instance.
		 *
		 * @param \Illuminate\Foundation\Application  $app
		 * @return void
		 * @static 
		 */
		 public static function __construct($app){
			//Method inherited from \Illuminate\Support\Manager
			 \Orchestra\Memory\MemoryManager::__construct($app);
		 }

		/**
		 * Get a driver instance.
		 *
		 * @param string  $driver
		 * @return mixed
		 * @static 
		 */
		 public static function driver($driver = null){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Memory\MemoryManager::driver($driver);
		 }

		/**
		 * Register a custom driver creator Closure.
		 *
		 * @param string   $driver
		 * @param Closure  $callback
		 * @return \Illuminate\Support\Manager|static
		 * @static 
		 */
		 public static function extend($driver, $callback){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Memory\MemoryManager::extend($driver, $callback);
		 }

		/**
		 * Get all of the created "drivers".
		 *
		 * @return array
		 * @static 
		 */
		 public static function getDrivers(){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Memory\MemoryManager::getDrivers();
		 }

		/**
		 * Dynamically call the default driver instance.
		 *
		 * @param string  $method
		 * @param array   $parameters
		 * @return mixed
		 * @static 
		 */
		 public static function __call($method, $parameters){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Memory\MemoryManager::__call($method, $parameters);
		 }

	}
	class Messages extends \Orchestra\Support\Facades\Messages{
		/**
		 * Set the session store.
		 *
		 * @param \Illuminate\Session\Store   $session
		 * @return Messages
		 * @static 
		 */
		 public static function setSession($session){
			//Method inherited from \Orchestra\Support\Messages
			return \Orchestra\Support\Messages::setSession($session);
		 }

		/**
		 * Get the session store.
		 *
		 * @return \Illuminate\Session\Store
		 * @static 
		 */
		 public static function getSession(){
			//Method inherited from \Orchestra\Support\Messages
			return \Orchestra\Support\Messages::getSession();
		 }

		/**
		 * Retrieve Message instance from Session, the data should be in
		 * serialize, so we need to unserialize it first.
		 *
		 * @return Messages
		 * @static 
		 */
		 public static function retrieve(){
			//Method inherited from \Orchestra\Support\Messages
			return \Orchestra\Support\Messages::retrieve();
		 }

		/**
		 * Extend Messages instance from session.
		 *
		 * @param \Closure $callback
		 * @return Messages
		 * @static 
		 */
		 public static function extend($callback){
			//Method inherited from \Orchestra\Support\Messages
			return \Orchestra\Support\Messages::extend($callback);
		 }

		/**
		 * Store current instance.
		 *
		 * @return void
		 * @static 
		 */
		 public static function save(){
			//Method inherited from \Orchestra\Support\Messages
			 \Orchestra\Support\Messages::save();
		 }

		/**
		 * Compile the instance into serialize.
		 *
		 * @return string   serialize of this instance
		 * @static 
		 */
		 public static function serialize(){
			//Method inherited from \Orchestra\Support\Messages
			return \Orchestra\Support\Messages::serialize();
		 }

		/**
		 * Create a new message bag instance.
		 *
		 * @param array  $messages
		 * @return void
		 * @static 
		 */
		 public static function __construct($messages = array()){
			//Method inherited from \Illuminate\Support\MessageBag
			 \Orchestra\Support\Messages::__construct($messages);
		 }

		/**
		 * Add a message to the bag.
		 *
		 * @param string  $key
		 * @param string  $message
		 * @return \Illuminate\Support\MessageBag
		 * @static 
		 */
		 public static function add($key, $message){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::add($key, $message);
		 }

		/**
		 * Merge a new array of messages into the bag.
		 *
		 * @param \Illuminate\Support\Contracts\MessageProviderInterface|array  $messages
		 * @return \Illuminate\Support\MessageBag
		 * @static 
		 */
		 public static function merge($messages){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::merge($messages);
		 }

		/**
		 * Determine if messages exist for a given key.
		 *
		 * @param string  $key
		 * @return bool
		 * @static 
		 */
		 public static function has($key = null){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::has($key);
		 }

		/**
		 * Get the first message from the bag for a given key.
		 *
		 * @param string  $key
		 * @param string  $format
		 * @return string
		 * @static 
		 */
		 public static function first($key = null, $format = null){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::first($key, $format);
		 }

		/**
		 * Get all of the messages from the bag for a given key.
		 *
		 * @param string  $key
		 * @param string  $format
		 * @return array
		 * @static 
		 */
		 public static function get($key, $format = null){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::get($key, $format);
		 }

		/**
		 * Get all of the messages for every key in the bag.
		 *
		 * @param string  $format
		 * @return array
		 * @static 
		 */
		 public static function all($format = null){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::all($format);
		 }

		/**
		 * Get the raw messages in the container.
		 *
		 * @return array
		 * @static 
		 */
		 public static function getMessages(){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::getMessages();
		 }

		/**
		 * Get the messages for the instance.
		 *
		 * @return \Illuminate\Support\MessageBag
		 * @static 
		 */
		 public static function getMessageBag(){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::getMessageBag();
		 }

		/**
		 * Get the default message format.
		 *
		 * @return string
		 * @static 
		 */
		 public static function getFormat(){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::getFormat();
		 }

		/**
		 * Set the default message format.
		 *
		 * @param string  $format
		 * @return \Illuminate\Support\MessageBag
		 * @static 
		 */
		 public static function setFormat($format = ':message'){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::setFormat($format);
		 }

		/**
		 * Determine if the message bag has any messages.
		 *
		 * @return bool
		 * @static 
		 */
		 public static function isEmpty(){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::isEmpty();
		 }

		/**
		 * Determine if the message bag has any messages.
		 *
		 * @return bool
		 * @static 
		 */
		 public static function any(){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::any();
		 }

		/**
		 * Get the number of messages in the container.
		 *
		 * @return int
		 * @static 
		 */
		 public static function count(){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::count();
		 }

		/**
		 * Get the instance as an array.
		 *
		 * @return array
		 * @static 
		 */
		 public static function toArray(){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::toArray();
		 }

		/**
		 * Convert the object to its JSON representation.
		 *
		 * @param int  $options
		 * @return string
		 * @static 
		 */
		 public static function toJson($options = 0){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::toJson($options);
		 }

		/**
		 * Convert the message bag to its string representation.
		 *
		 * @return string
		 * @static 
		 */
		 public static function __toString(){
			//Method inherited from \Illuminate\Support\MessageBag
			return \Orchestra\Support\Messages::__toString();
		 }

	}
	class Notifier extends \Orchestra\Support\Facades\Notifier{
		/**
		 * Get the default driver.
		 *
		 * @return string
		 * @static 
		 */
		 public static function getDefaultDriver(){
			//Method inherited from \Orchestra\Notifier\NotifierManager
			return \Orchestra\Notifier\NotifierManager::getDefaultDriver();
		 }

		/**
		 * Set the default driver.
		 *
		 * @param string  $name
		 * @return string
		 * @static 
		 */
		 public static function setDefaultDriver($name){
			//Method inherited from \Orchestra\Notifier\NotifierManager
			return \Orchestra\Notifier\NotifierManager::setDefaultDriver($name);
		 }

		/**
		 * Create a new manager instance.
		 *
		 * @param \Illuminate\Foundation\Application  $app
		 * @return void
		 * @static 
		 */
		 public static function __construct($app){
			//Method inherited from \Illuminate\Support\Manager
			 \Orchestra\Notifier\NotifierManager::__construct($app);
		 }

		/**
		 * Get a driver instance.
		 *
		 * @param string  $driver
		 * @return mixed
		 * @static 
		 */
		 public static function driver($driver = null){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Notifier\NotifierManager::driver($driver);
		 }

		/**
		 * Register a custom driver creator Closure.
		 *
		 * @param string   $driver
		 * @param Closure  $callback
		 * @return \Illuminate\Support\Manager|static
		 * @static 
		 */
		 public static function extend($driver, $callback){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Notifier\NotifierManager::extend($driver, $callback);
		 }

		/**
		 * Get all of the created "drivers".
		 *
		 * @return array
		 * @static 
		 */
		 public static function getDrivers(){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Notifier\NotifierManager::getDrivers();
		 }

		/**
		 * Dynamically call the default driver instance.
		 *
		 * @param string  $method
		 * @param array   $parameters
		 * @return mixed
		 * @static 
		 */
		 public static function __call($method, $parameters){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Notifier\NotifierManager::__call($method, $parameters);
		 }

	}
	class Profiler extends \Orchestra\Support\Facades\Profiler{
		/**
		 * Construct a new instance.
		 *
		 * @param \Illuminate\Container\Container  $container
		 * @param \Monolog\Logger                  $monolog
		 * @static 
		 */
		 public static function __construct($container, $monolog){
			//Method inherited from \Orchestra\Debug\Profiler
			 \Orchestra\Debug\Profiler::__construct($container, $monolog);
		 }

		/**
		 * Attach the debugger.
		 *
		 * @return Profiler
		 * @static 
		 */
		 public static function attachDebugger(){
			//Method inherited from \Orchestra\Debug\Profiler
			return \Orchestra\Debug\Profiler::attachDebugger();
		 }

		/**
		 * Extend the profiler.
		 *
		 * @param \Closure    $callback
		 * @return void
		 * @static 
		 */
		 public static function extend($callback){
			//Method inherited from \Orchestra\Debug\Profiler
			 \Orchestra\Debug\Profiler::extend($callback);
		 }

		/**
		 * Set the event dispatcher instance to be used by connections.
		 *
		 * @param \Illuminate\Events\Dispatcher $dispatcher
		 * @return void
		 * @static 
		 */
		 public static function setEventDispatcher($dispatcher){
			//Method inherited from \Orchestra\Debug\Profiler
			 \Orchestra\Debug\Profiler::setEventDispatcher($dispatcher);
		 }

		/**
		 * Get the current event dispatcher instance.
		 *
		 * @return \Illuminate\Events\Dispatcher
		 * @static 
		 */
		 public static function getEventDispatcher(){
			//Method inherited from \Orchestra\Debug\Profiler
			return \Orchestra\Debug\Profiler::getEventDispatcher();
		 }

		/**
		 * Get monolog instance.
		 *
		 * @return \Monolog\Logger
		 * @static 
		 */
		 public static function getMonolog(){
			//Method inherited from \Orchestra\Debug\Profiler
			return \Orchestra\Debug\Profiler::getMonolog();
		 }

	}
	class Publisher extends \Orchestra\Support\Facades\Publisher{
		/**
		 * Create a new manager instance.
		 *
		 * @param \Illuminate\Foundation\Application  $app
		 * @return void
		 * @static 
		 */
		 public static function __construct($app){
			//Method inherited from \Orchestra\Foundation\Publisher\PublisherManager
			 \Orchestra\Foundation\Publisher\PublisherManager::__construct($app);
		 }

		/**
		 * Execute the queue.
		 *
		 * @return boolean
		 * @static 
		 */
		 public static function execute(){
			//Method inherited from \Orchestra\Foundation\Publisher\PublisherManager
			return \Orchestra\Foundation\Publisher\PublisherManager::execute();
		 }

		/**
		 * Add a process to be queue.
		 *
		 * @param string   $queue
		 * @return boolean
		 * @static 
		 */
		 public static function queue($queue){
			//Method inherited from \Orchestra\Foundation\Publisher\PublisherManager
			return \Orchestra\Foundation\Publisher\PublisherManager::queue($queue);
		 }

		/**
		 * Get a current queue.
		 *
		 * @return array
		 * @static 
		 */
		 public static function queued(){
			//Method inherited from \Orchestra\Foundation\Publisher\PublisherManager
			return \Orchestra\Foundation\Publisher\PublisherManager::queued();
		 }

		/**
		 * Get a driver instance.
		 *
		 * @param string  $driver
		 * @return mixed
		 * @static 
		 */
		 public static function driver($driver = null){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Foundation\Publisher\PublisherManager::driver($driver);
		 }

		/**
		 * Register a custom driver creator Closure.
		 *
		 * @param string   $driver
		 * @param Closure  $callback
		 * @return \Illuminate\Support\Manager|static
		 * @static 
		 */
		 public static function extend($driver, $callback){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Foundation\Publisher\PublisherManager::extend($driver, $callback);
		 }

		/**
		 * Get all of the created "drivers".
		 *
		 * @return array
		 * @static 
		 */
		 public static function getDrivers(){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Foundation\Publisher\PublisherManager::getDrivers();
		 }

		/**
		 * Dynamically call the default driver instance.
		 *
		 * @param string  $method
		 * @param array   $parameters
		 * @return mixed
		 * @static 
		 */
		 public static function __call($method, $parameters){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Foundation\Publisher\PublisherManager::__call($method, $parameters);
		 }

	}
	class Resources extends \Orchestra\Support\Facades\Resources{
		/**
		 * Construct a new Resources instance.
		 *
		 * @param \Orchestra\Resources\Dispatcher $dispatcher
		 * @param \Orchestra\Resources\Response   $response
		 * @static 
		 */
		 public static function __construct($dispatcher, $response){
			//Method inherited from \Orchestra\Resources\Environment
			 \Orchestra\Resources\Environment::__construct($dispatcher, $response);
		 }

		/**
		 * Register a new resource.
		 *
		 * @param string   $name
		 * @param mixed    $attributes
		 * @return \Orchestra\Resources\Container
		 * @static 
		 */
		 public static function make($name, $attributes){
			//Method inherited from \Orchestra\Resources\Environment
			return \Orchestra\Resources\Environment::make($name, $attributes);
		 }

		/**
		 * Get resource by given name, or create a new one.
		 *
		 * @param string   $name
		 * @param mixed    $attributes
		 * @return \Orchestra\Resources\Container
		 * @static 
		 */
		 public static function of($name, $attributes = null){
			//Method inherited from \Orchestra\Resources\Environment
			return \Orchestra\Resources\Environment::of($name, $attributes);
		 }

		/**
		 * Call a resource controller and action.
		 *
		 * @param string   $name
		 * @param array    $parameters
		 * @return \Orchestra\Resources\Response
		 * @static 
		 */
		 public static function call($name, $parameters = array()){
			//Method inherited from \Orchestra\Resources\Environment
			return \Orchestra\Resources\Environment::call($name, $parameters);
		 }

		/**
		 * Handle response from resources.
		 *
		 * @param mixed    $content
		 * @param Closure  $callback
		 * @return mixed
		 * @static 
		 */
		 public static function response($content, $callback = null){
			//Method inherited from \Orchestra\Resources\Environment
			return \Orchestra\Resources\Environment::response($content, $callback);
		 }

		/**
		 * Get all registered resources.
		 *
		 * @return array
		 * @static 
		 */
		 public static function all(){
			//Method inherited from \Orchestra\Resources\Environment
			return \Orchestra\Resources\Environment::all();
		 }

	}
	class Site extends \Orchestra\Support\Facades\Site{
		/**
		 * Construct a new instance.
		 *
		 * @param \Illuminate\Auth\AuthManager    $auth
		 * @param \Illuminate\Config\Repository   $config
		 * @param \Orchestra\Memory\Provider      $memory
		 * @static 
		 */
		 public static function __construct($auth, $config, $memory){
			//Method inherited from \Orchestra\Foundation\Site
			 \Orchestra\Foundation\Site::__construct($auth, $config, $memory);
		 }

		/**
		 * Convert given time to user localtime, however if it a guest user
		 * return based on default timezone.
		 *
		 * @param mixed    $datetime
		 * @return \Carbon\Carbon
		 * @static 
		 */
		 public static function toLocalTime($datetime){
			//Method inherited from \Orchestra\Foundation\Site
			return \Orchestra\Foundation\Site::toLocalTime($datetime);
		 }

		/**
		 * Convert given time to user from localtime, however if it a guest user
		 * return based on default timezone.
		 *
		 * @param mixed    $datetime
		 * @return \Carbon\Carbon
		 * @static 
		 */
		 public static function fromLocalTime($datetime){
			//Method inherited from \Orchestra\Foundation\Site
			return \Orchestra\Foundation\Site::fromLocalTime($datetime);
		 }

		/**
		 * Convert datetime string to DateTime.
		 *
		 * @param mixed    $datetime
		 * @param string   $timezone
		 * @return \Carbon\Carbon
		 * @static 
		 */
		 public static function convertToDateTime($datetime, $timezone = null){
			//Method inherited from \Orchestra\Foundation\Site
			return \Orchestra\Foundation\Site::convertToDateTime($datetime, $timezone);
		 }

		/**
		 * Get a site value.
		 *
		 * @param string   $key
		 * @param mixed    $default
		 * @return mixed
		 * @static 
		 */
		 public static function get($key, $default = null){
			//Method inherited from \Orchestra\Support\Relic
			return \Orchestra\Foundation\Site::get($key, $default);
		 }

		/**
		 * Set a site value.
		 *
		 * @param string   $key
		 * @param mixed    $value
		 * @return mixed
		 * @static 
		 */
		 public static function set($key, $value = null){
			//Method inherited from \Orchestra\Support\Relic
			return \Orchestra\Foundation\Site::set($key, $value);
		 }

		/**
		 * Check if site key has a value.
		 *
		 * @param string   $key
		 * @return boolean
		 * @static 
		 */
		 public static function has($key){
			//Method inherited from \Orchestra\Support\Relic
			return \Orchestra\Foundation\Site::has($key);
		 }

		/**
		 * Remove a site key.
		 *
		 * @param string   $key
		 * @return void
		 * @static 
		 */
		 public static function forget($key){
			//Method inherited from \Orchestra\Support\Relic
			 \Orchestra\Foundation\Site::forget($key);
		 }

		/**
		 * Get all available items.
		 *
		 * @return array
		 * @static 
		 */
		 public static function all(){
			//Method inherited from \Orchestra\Support\Relic
			return \Orchestra\Foundation\Site::all();
		 }

	}
	class Table extends \Orchestra\Support\Facades\Table{
		/**
		 * Create a new Builder instance.
		 *
		 * @param \Closure $callback
		 * @return object
		 * @static 
		 */
		 public static function make($callback){
			//Method inherited from \Orchestra\Html\Table\Environment
			return \Orchestra\Html\Table\Environment::make($callback);
		 }

		/**
		 * Construct a new environment.
		 *
		 * @param \Illuminate\Container\Container  $app
		 * @static 
		 */
		 public static function __construct($app){
			//Method inherited from \Orchestra\Html\Abstractable\Environment
			 \Orchestra\Html\Table\Environment::__construct($app);
		 }

		/**
		 * Create a new builder instance of a named builder.
		 *
		 * @param string   $name
		 * @param \Closure $callback
		 * @return object
		 * @static 
		 */
		 public static function of($name, $callback = null){
			//Method inherited from \Orchestra\Html\Abstractable\Environment
			return \Orchestra\Html\Table\Environment::of($name, $callback);
		 }

	}
	class Theme extends \Orchestra\Support\Facades\Theme{
		/**
		 * Detect available themes.
		 *
		 * @return array
		 * @static 
		 */
		 public static function detect(){
			//Method inherited from \Orchestra\View\Theme\ThemeManager
			return \Orchestra\View\Theme\ThemeManager::detect();
		 }

		/**
		 * Create a new manager instance.
		 *
		 * @param \Illuminate\Foundation\Application  $app
		 * @return void
		 * @static 
		 */
		 public static function __construct($app){
			//Method inherited from \Illuminate\Support\Manager
			 \Orchestra\View\Theme\ThemeManager::__construct($app);
		 }

		/**
		 * Get a driver instance.
		 *
		 * @param string  $driver
		 * @return mixed
		 * @static 
		 */
		 public static function driver($driver = null){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\View\Theme\ThemeManager::driver($driver);
		 }

		/**
		 * Register a custom driver creator Closure.
		 *
		 * @param string   $driver
		 * @param Closure  $callback
		 * @return \Illuminate\Support\Manager|static
		 * @static 
		 */
		 public static function extend($driver, $callback){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\View\Theme\ThemeManager::extend($driver, $callback);
		 }

		/**
		 * Get all of the created "drivers".
		 *
		 * @return array
		 * @static 
		 */
		 public static function getDrivers(){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\View\Theme\ThemeManager::getDrivers();
		 }

		/**
		 * Dynamically call the default driver instance.
		 *
		 * @param string  $method
		 * @param array   $parameters
		 * @return mixed
		 * @static 
		 */
		 public static function __call($method, $parameters){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\View\Theme\ThemeManager::__call($method, $parameters);
		 }

	}
	class Widget extends \Orchestra\Support\Facades\Widget{
		/**
		 * Get the default driver.
		 *
		 * @return string
		 * @static 
		 */
		 public static function getDefaultDriver(){
			//Method inherited from \Orchestra\Widget\WidgetManager
			return \Orchestra\Widget\WidgetManager::getDefaultDriver();
		 }

		/**
		 * Set the default driver.
		 *
		 * @param string   $name
		 * @return void
		 * @static 
		 */
		 public static function setDefaultDriver($name){
			//Method inherited from \Orchestra\Widget\WidgetManager
			 \Orchestra\Widget\WidgetManager::setDefaultDriver($name);
		 }

		/**
		 * Get the selected driver and extend it via callback.
		 *
		 * @param string   $name
		 * @param \Closure $callback
		 * @return Factory
		 * @static 
		 */
		 public static function of($name, $callback = null){
			//Method inherited from \Orchestra\Widget\WidgetManager
			return \Orchestra\Widget\WidgetManager::of($name, $callback);
		 }

		/**
		 * Create a new instance.
		 *
		 * @param string   $driver
		 * @return object
		 * @see Manager::driver()
		 * @static 
		 */
		 public static function make($driver = null){
			//Method inherited from \Orchestra\Support\Manager
			return \Orchestra\Widget\WidgetManager::make($driver);
		 }

		/**
		 * Create a new manager instance.
		 *
		 * @param \Illuminate\Foundation\Application  $app
		 * @return void
		 * @static 
		 */
		 public static function __construct($app){
			//Method inherited from \Illuminate\Support\Manager
			 \Orchestra\Widget\WidgetManager::__construct($app);
		 }

		/**
		 * Get a driver instance.
		 *
		 * @param string  $driver
		 * @return mixed
		 * @static 
		 */
		 public static function driver($driver = null){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Widget\WidgetManager::driver($driver);
		 }

		/**
		 * Register a custom driver creator Closure.
		 *
		 * @param string   $driver
		 * @param Closure  $callback
		 * @return \Illuminate\Support\Manager|static
		 * @static 
		 */
		 public static function extend($driver, $callback){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Widget\WidgetManager::extend($driver, $callback);
		 }

		/**
		 * Get all of the created "drivers".
		 *
		 * @return array
		 * @static 
		 */
		 public static function getDrivers(){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Widget\WidgetManager::getDrivers();
		 }

		/**
		 * Dynamically call the default driver instance.
		 *
		 * @param string  $method
		 * @param array   $parameters
		 * @return mixed
		 * @static 
		 */
		 public static function __call($method, $parameters){
			//Method inherited from \Illuminate\Support\Manager
			return \Orchestra\Widget\WidgetManager::__call($method, $parameters);
		 }

	}
}